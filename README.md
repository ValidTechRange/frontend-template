# Frontend Template


## Core Packages
* [React](https://facebook.github.io/react/)
* [MobX](https://mobx.js.org/)
* [React Router](https://reacttraining.com/react-router/web/guides/philosophy)
* [Semantic UI React](https://react.semantic-ui.com/introduction)


## Quick Start Instructions
1. Clone this repo
    * ```git clone https://ValidTechRange@bitbucket.org/ValidTechRange/frontend-template.git```
2. Delete the .git folder
    * ```rm -rf frontend-template/.git```
3. Rename folder to new project
    * ```mv frontend-template MyProject```
4. Change directory to this repo
    * ```cd MyProject```
5. Install webpack globally
    * ```npm install -g webpack```
6. Install all packages
    * ```npm install```
6. Change package.json properties
    * Name
    * Version
    * Description
    * Author
    * License
7. Start the webpack-dev-server
    * ```npm start```
8. Navigate to page
    * [http://localhost:8081](http://localhost:8081)


## Bundle Instructions
In order to serve this frontend code, you will need to bundle it and serve it with your favorite static file server.

1. Run: ```webpack```
2. Copy dist/ **AND index.html** to your static files directory. You need to keep the same directory structure:
    * Root/
        * dist/
            * src/
            * bundle.js
            * bundle.js.map
            * config.js
        * index.html
