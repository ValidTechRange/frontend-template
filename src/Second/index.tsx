import * as React from 'react';
import {Grid} from "semantic-ui-react";

export class Second extends React.Component<any, any> {
    render() {
        return (
            <Grid>
                <Grid.Row columns={1}>
                    <h1>Second</h1>
                </Grid.Row>
            </Grid>
        );
    }
}
