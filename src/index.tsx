import * as React from "react";
import * as ReactDOM from "react-dom";

import {Route, Switch} from 'react-router-dom';
import {BrowserRouter} from "react-router-dom";
import {useStrict} from "mobx";
import DevTools from 'mobx-react-devtools';
import {Container, Grid, Menu} from "semantic-ui-react";

import {MenuItem} from './utils';
import config from '../config';

import {Home} from './Home';
import {Second} from './Second';

useStrict(true);
ReactDOM.render(
    <BrowserRouter>
        <Container>
            <Grid>
                <Grid.Row columns={1} centered>
                    {config.dev ? (<DevTools />) : null }
                </Grid.Row>
                <Grid.Row>
                    <Menu fluid>
                        <MenuItem to="/" content="Home" />
                        <MenuItem to="/second" content="Second"/>
                    </Menu>
                </Grid.Row>
            </Grid>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/second" component={Second} />
            </Switch>
        </Container>
    </BrowserRouter>,
    document.getElementById("app")
);
