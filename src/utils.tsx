import * as React from 'react';
import {NavLink} from 'react-router-dom';
import {Menu} from 'semantic-ui-react';

function _matchPath(match: any, location: any) {
    if (match){
        return (match.path != "/" && !!match) ||
            (match.path == "/" && (match.isExact || location.pathname.startsWith("/home/")));
    } else {
        return false;
    }
}

const _menuProps = {
    as: NavLink,
    isActive: _matchPath,
    activeClassName: "active"
};

export class MenuItem extends React.Component<{to: string, content?: string}, any> {
    render() {
        return (
            <Menu.Item {..._menuProps} {...this.props} />
        );
    }
}